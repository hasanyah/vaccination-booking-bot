package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gocolly/colly/v2"
)

const DOMAIN string = "vgregion.se"
const URL string = "https://www.vgregion.se/ov/vaccinationstider/bokningsbara-tider/"

// TODO:read from config file
// const TELEGRAM_URL string = ""

func main() {
	inputFileName := "vaccinationPlaces.txt"
	fileName := flag.String("inputFile", inputFileName, "Relative file path to use as input.")
	flag.Parse()
	file, err := os.OpenFile(*fileName, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	places := GetSavedPlaces(file)
	fmt.Println("File contains: ")
	fmt.Println(places)
	file.Truncate(0)
	file.Seek(0, 0)

	collector := colly.NewCollector()
	SetupCollector(collector, places, file)
	collector.Visit(URL)

	return
}

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func SendNotification(message string) {
	botLink := TELEGRAM_URL + "&text=" + strings.ReplaceAll(message, " ", "+")
	fmt.Println(botLink)
	resp, err := http.Get(botLink)
	fmt.Println(resp)
	if err != nil {
		fmt.Println(err)
	}
}

func GetSavedPlaces(file *os.File) []string {
	reader := bufio.NewReader(file)
	places := []string{}
	for {
		line, err := reader.ReadString('\n')
		if err != nil || len(line) == 0 {
			break
		}
		line = strings.TrimSpace(line)
		places = append(places, line)
	}
	return places
}

func SetupCollector(collector *colly.Collector, places []string, file *os.File) {
	collector.OnHTML(".mottagningbookabletimeslistblock .media-body", func(h *colly.HTMLElement) {
		title := strings.TrimSpace(h.ChildText("h3"))
		link := h.ChildAttr("a", "href")
		// details := h.ChildText("span")
		// fmt.Printf("%q\n %v\n %v\n", title, details, link)
		if !Contains(places, title) {
			SendNotification(title + "%0A" + link)
		}
		if _, err := file.WriteString(title + "\n"); err != nil {
			log.Println(err)
		}
	})

	// Before making a request print "Visiting ..."
	collector.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})
}
